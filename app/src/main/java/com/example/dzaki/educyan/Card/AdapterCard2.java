package com.example.dzaki.educyan.Card;



import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dzaki.educyan.R;

import java.util.ArrayList;

public class AdapterCard2 extends RecyclerView.Adapter<AdapterCard2.ViewHolder> {
    private ArrayList<MateriCard> daftarMateri;
    private Context mContext;

    public AdapterCard2(ArrayList<MateriCard> daftarMateri, Context mContext) {
        this.daftarMateri = daftarMateri;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AdapterCard2.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.card2,viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCard2.ViewHolder viewHolder, int i) {
        MateriCard card = daftarMateri.get(i);
        viewHolder.bindTo(card);
    }

    @Override
    public int getItemCount() {
        return daftarMateri.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView judul;
//        private ImageView gambar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            judul = itemView.findViewById(R.id.txJudulMateri);
//            desc = itemView.findViewById(R.id.txDesc);
//            gambar = itemView.findViewById(R.id.imageFoto);

            itemView.setOnClickListener(this);
        }


        public void bindTo(MateriCard card){
            judul.setText(card.getJudul());
        }
        @Override
        public void onClick(View v) {
            Toast.makeText(mContext, judul.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }
}

