package com.example.dzaki.educyan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class HomeAdmin extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Feedback> daftarFeedback;
    AdapterFeedback adapterFeedback;
    FirebaseAuth menAuth;
    DatabaseReference reference;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        menAuth = FirebaseAuth.getInstance();
        recyclerView = findViewById(R.id.rvFeedback);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        daftarFeedback = new ArrayList<>();
        adapterFeedback = new AdapterFeedback(daftarFeedback,this);
        recyclerView.setAdapter(adapterFeedback);
        reference = FirebaseDatabase.getInstance().getReference().child("feedback");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    daftarFeedback.clear();
                    for (DataSnapshot x : dataSnapshot.getChildren()){
                        daftarFeedback.add(new Feedback(x.child("feedback").getValue().toString(),x.child("author").getValue().toString()));
                    }
                    adapterFeedback.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menuadmin,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout :
                menAuth.signOut();
                startActivity(new Intent(this,MainActivity.class));
                finish();
                return true;

            default :
                return super.onOptionsItemSelected(item);
        }

    }
}
