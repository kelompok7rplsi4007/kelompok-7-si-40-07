package com.example.dzaki.educyan.Card;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dzaki.educyan.DetailActivity;
import com.example.dzaki.educyan.R;

import java.util.ArrayList;

public class AdapterMateri extends RecyclerView.Adapter<AdapterMateri.ViewHolder> {
    private ArrayList<MateriCard> daftarMateri;
    private Context mContext;

    public AdapterMateri(ArrayList<MateriCard> daftarMateri, Context mContext) {
        this.daftarMateri = daftarMateri;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
//        return new AdapterMateri.ViewHolder;
        return new AdapterMateri.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.card,viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterMateri.ViewHolder viewHolder, int i) {
        MateriCard card = daftarMateri.get(i);
        viewHolder.bindTo(card);
    }

    @Override
    public int getItemCount() {
        return daftarMateri.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView judul, desc;
        private ImageView gambar;
        String author;
        String deskripsi, level,key;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            judul = itemView.findViewById(R.id.txJudul);
            desc = itemView.findViewById(R.id.txDesc);
            gambar = itemView.findViewById(R.id.imageFoto);


            itemView.setOnClickListener(this);
        }


        @SuppressLint("SetTextI18n")
        public void bindTo(MateriCard card){
            judul.setText(card.getJudul());

            deskripsi = card.getDeskripsi();
            level = card.getKelas();
            key = card.getId();
            if (card.getDeskripsi().length()>12){
                desc.setText(card.getDeskripsi().substring(0,12)+"...");
            }else{
                desc.setText(card.getDeskripsi());
            }

            author = card.getAuthor();
//            try {
//                gambar.setImageResource(card.getIdFoto());
//            }catch (Exception e){
//
//            }
        }
        @Override
        public void onClick(View v) {
            Toast.makeText(mContext, judul.getText().toString(), Toast.LENGTH_SHORT).show();
            Intent in = new Intent(mContext, DetailActivity.class);
            in.putExtra("judul",judul.getText().toString());
            in.putExtra("desc",deskripsi);
            in.putExtra("author",author);
            in.putExtra("level",level);
            in.putExtra("id",key);
            mContext.startActivity(in);
        }
    }
}
