package com.example.dzaki.educyan;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dzaki.educyan.Card.AdapterModul;
import com.example.dzaki.educyan.Card.Modul;
import com.example.dzaki.educyan.Card.Soal.AdapterSoal;
import com.example.dzaki.educyan.Card.Soal.Soal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class DetailActivity extends AppCompatActivity {
    TextView judul, author, desc,bioX;
    String level,key;

    RecyclerView rvmodule,rvQuiz;
    ArrayList<Modul> daftarModul;
    ArrayList<Soal>daftarSoal;
    AdapterModul adapterModul;
    AdapterSoal adapterSoal;
    Query ref;

    boolean nullRef, owner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theme();
        setContentView(R.layout.activity_detail);
        judul = findViewById(R.id.txJudul);
        author = findViewById(R.id.txAuthor);
        desc = findViewById(R.id.txdesc);
        rvmodule = findViewById(R.id.rvModule);
        rvQuiz = findViewById(R.id.rvSoal);
        rvQuiz.setLayoutManager(new LinearLayoutManager(this));
        rvmodule.setLayoutManager(new LinearLayoutManager(this));
        daftarModul = new ArrayList<>();
        daftarSoal = new ArrayList<>();
        bioX = findViewById(R.id.txBio);




        judul.setText(getIntent().getStringExtra("judul"));
        desc.setText(getIntent().getStringExtra("desc"));
        level = getIntent().getStringExtra("level");
        key = getIntent().getStringExtra("id");

        adapterModul = new AdapterModul(daftarModul,this,level,key,getIntent().getStringExtra("author"));
        adapterSoal = new AdapterSoal(daftarSoal,DetailActivity.this);
        rvmodule.setAdapter(adapterModul);
        rvQuiz.setAdapter(adapterSoal);
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("User").child(getIntent().getStringExtra("author"));
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                Log.d("author",dataSnapshot.getValue().toString());
                author.setText(dataSnapshot.child("fullname").getValue().toString());
                if (dataSnapshot.child("bio").exists()){
                    bioX.setText(dataSnapshot.child("bio").getValue().toString());
                }else{
                    bioX.setText("-");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
//        try{
            ref = FirebaseDatabase.getInstance().getReference().child("materi").child(level).child(key).child("modul").orderByChild("time");
//            if (!ref.toString().equals(null)){
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    daftarModul.clear();
                    adapterModul.setShow(owner);
                    for (DataSnapshot x: dataSnapshot.getChildren()) {
                        daftarModul.add(new Modul(x.getKey(),x.child("isi").getValue().toString()));
                        adapterModul.notifyDataSetChanged();
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        DatabaseReference refSoal = FirebaseDatabase.getInstance().getReference().child("materi").child(level).child(key).child("quiz");
        refSoal.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    daftarSoal.clear();
                    for (DataSnapshot x : dataSnapshot.getChildren()){
                        daftarSoal.add(new Soal(x.child("soal").getValue().toString(),x.child("A").getValue().toString(),x.child("B").getValue().toString(),x.child("C").getValue().toString(),x.child("Jawaban").getValue().toString()));
                    }
                    adapterSoal.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public void chat(View view){
        startActivity(new Intent(DetailActivity.this,MainMess.class));
    }

    private void theme() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean("nightMode", false)) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("User").child(auth.getCurrentUser().getUid());
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("status").getValue().equals("teacher")){
                    DatabaseReference mref = FirebaseDatabase.getInstance().getReference().child("materi").child(level).child(key);
                    mref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.child("author").getValue().toString().equals(auth.getCurrentUser().getUid())){
                                owner = true;

                                MenuInflater menuInflater = getMenuInflater();
                                menuInflater.inflate(R.menu.course,menu);
                            }else{
                                owner = false;
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
//                    Toast.makeText(DetailActivity.this, "Teacher", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    Dialog module;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.addModule :
                module = new Dialog(this);
                module.setContentView(R.layout.module);
                    final EditText namaModul = module.findViewById(R.id.edModulName);
                    final EditText isiModul = module.findViewById(R.id.edIsiModule);
                    Button add = module.findViewById(R.id.add);
                    Button cancel = module.findViewById(R.id.cancel);

                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("materi").child(level).child(key).child("modul").child(namaModul.getText().toString());
                            ref.child("isi").setValue(isiModul.getText().toString());
                            SimpleDateFormat fom = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                            ref.child("time").setValue(fom.format(new Date()));
//                            if (nullRef==true){
//                                tryagain();
//                            }
                            module.dismiss();
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            module.dismiss();
                        }
                    });
                module.show();
                break;
            case R.id.addQuiz:
                Intent in = new Intent(DetailActivity.this,QuizDbHelperJadi.class);
                    in.putExtra("level",level);
                    in.putExtra("kelas",key);
                startActivity(in);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void feedback(View view){
        startActivity(new Intent(this,InputFeedback.class));
    }
}
