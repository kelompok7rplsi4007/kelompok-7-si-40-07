package com.example.dzaki.educyan.Card;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dzaki.educyan.DetailActivity;
import com.example.dzaki.educyan.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AdapterModul extends RecyclerView.Adapter<AdapterModul.ViewHolder> {
    private ArrayList<Modul> daftarModul;
    private Context mContext;
    private boolean show;
    private Dialog module;
    private String level, key,author;

    public void setShow(boolean show) {
        this.show = show;
    }

    public AdapterModul(ArrayList<Modul> daftarModul, Context mContext, String level, String key, String author) {
        this.daftarModul = daftarModul;
        this.mContext = mContext;
        this.level = level;
        this.key = key;
        this.author = author;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AdapterModul.ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.modulcard,viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Modul card = daftarModul.get(i);
        viewHolder.bindTo(card);
    }

    @Override
    public int getItemCount() {
        return daftarModul.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView judul, desc;
        private boolean show = true;
        private Button edit, delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            judul = itemView.findViewById(R.id.txNamaModul);
            desc  = itemView.findViewById(R.id.isiModul);
            edit = itemView.findViewById(R.id.edButton);
            delete = itemView.findViewById(R.id.delButton);

            FirebaseAuth auth = FirebaseAuth.getInstance();

            if (!auth.getCurrentUser().getUid().equals(author)){
                edit.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(this);
        }


        @SuppressLint("SetTextI18n")
        public void bindTo(final Modul card){
            judul.setText(card.getJudul());
            desc.setText(card.getIsi());
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("materi").child(level).child(key).child("modul").child(judul.getText().toString());
                    ref.removeValue();
                    daftarModul.clear();
                    notifyDataSetChanged();
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    module = new Dialog(mContext);
                    module.setContentView(R.layout.module);
                    final EditText namaModul = module.findViewById(R.id.edModulName);
                    final EditText isiModul = module.findViewById(R.id.edIsiModule);
                    Button add = module.findViewById(R.id.add);
                    add.setText("Edit");
                    Button cancel = module.findViewById(R.id.cancel);
                    namaModul.setText(card.getJudul());
                    isiModul.setText(card.getIsi());
                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("materi").child(level).child(key).child("modul").child(judul.getText().toString());
                            if (!judul.getText().toString().equals(namaModul.getText().toString())) {
                                ref.removeValue();
                                ref = FirebaseDatabase.getInstance().getReference().child("materi").child(level).child(key).child("modul").child(namaModul.getText().toString());
                            }
                                ref.child("isi").setValue(isiModul.getText().toString());
                                SimpleDateFormat fom = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
                                ref.child("time").setValue(fom.format(new Date()));

                            module.dismiss();
                        }
                    });
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            module.dismiss();
                        }
                    });
                    module.show();
                }
            });
        }
        @Override
        public void onClick(View v) {
            if (show){
                desc.setVisibility(View.GONE);
                show = false;
            }else{
                desc.setVisibility(View.VISIBLE);
                show = true;
            }
        }
    }

}
