package com.example.dzaki.educyan;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.dzaki.educyan.Card.AdapterMateri;
import com.example.dzaki.educyan.Card.MateriCard;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {
    FirebaseAuth menAuth;
    Dialog dialog;
    RadioGroup jenis;
    EditText judul, desk;
    Button del1, del2, del3;


    RecyclerView recyclerView, rv2, rv3;
    ArrayList<MateriCard> cardArrayList, ca2, ca3;
    AdapterMateri adapterMateri, am2, am3;
    ImageButton imageButton;

    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_first_teacher, container, false);
        del1 = view.findViewById(R.id.delsub);
        del2 = view.findViewById(R.id.delsub2);
        del3 = view.findViewById(R.id.delsub3);




        menAuth = FirebaseAuth.getInstance();
        if (menAuth.getCurrentUser() == null) {
            startActivity(new Intent(getContext(), MainActivity.class));
            getActivity().finish();
        }
        recyclerView = view.findViewById(R.id.menu1);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        cardArrayList = new ArrayList<>();
        adapterMateri = new AdapterMateri(cardArrayList, view.getContext());
        recyclerView.setAdapter(adapterMateri);
        rv2 = view.findViewById(R.id.menu2);
        rv2.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        ca2 = new ArrayList<>();
        am2 = new AdapterMateri(ca2, view.getContext());
        rv2.setAdapter(am2);
        rv3 = view.findViewById(R.id.menu3);
        rv3.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        ca3 = new ArrayList<>();
        am3 = new AdapterMateri(ca3, view.getContext());
        rv3.setAdapter(am3);
        initX(view);

        imageButton = view.findViewById(R.id.imageButton);
        del1.setVisibility(View.GONE);
        del2.setVisibility(View.GONE);
        del3.setVisibility(View.GONE);
        imageButton.setVisibility(View.GONE);

        return view;
    }

    private void theme() {
        SharedPreferences prefs = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
        if (prefs.getBoolean("nightMode", false)) {
            getActivity().setTheme(R.style.dark);
        } else {
            getActivity().setTheme(R.style.light);
        }
    }

    public void add(final View view) {
    }

    public void initX(View view) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("materi");
        ref.child("Matematika").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                cardArrayList.clear();
                for (DataSnapshot x : dataSnapshot.getChildren()) {
//                    imagesDir.add(imageSnapshot.child("address").getValue(String.class));
                    cardArrayList.add(new MateriCard("Matematika", x.getKey(), x.child("Judul").getValue().toString(), x.child("Deskripsi").getValue().toString(), x.child("author").getValue().toString()));
                    Log.d("LALALA -> " + x.getKey(), x.getValue().toString());
                }
                adapterMateri.notifyDataSetChanged();
//                Log.d("TSTT",dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR", databaseError.getMessage());
            }
        });


        ref.child("BInggris").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ca2.clear();
                for (DataSnapshot x : dataSnapshot.getChildren()) {
                    ca2.add(new MateriCard("BInggris", x.getKey(), x.child("Judul").getValue().toString(), x.child("Deskripsi").getValue().toString(), x.child("author").getValue().toString()));
                }
                am2.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR", databaseError.getMessage());
            }
        });


        ref.child("Computer Science").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ca3.clear();
                for (DataSnapshot x : dataSnapshot.getChildren()) {
                    ca3.add(new MateriCard("Computer Science", x.getKey(), x.child("Judul").getValue().toString(), x.child("Deskripsi").getValue().toString(), x.child("author").getValue().toString()));
                }
                am3.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR", databaseError.getMessage());
            }
        });

    }

}
