package com.example.dzaki.educyan;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;

import com.example.dzaki.educyan.Card.AdapterMateri;
import com.example.dzaki.educyan.Card.MateriCard;
import com.example.dzaki.educyan.Card.MenuFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class ActivityMenu extends AppCompatActivity {

    FirebaseAuth menAuth;
    RecyclerView recyclerView, rv2, rv3;
    ArrayList<MateriCard> cardArrayList, ca2,ca3;
    AdapterMateri adapterMateri, am2, am3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theme();
        setContentView(R.layout.activity_menu);
        menAuth = FirebaseAuth.getInstance();
        if (menAuth.getCurrentUser() == null) {
            startActivity(new Intent(ActivityMenu.this, MainActivity.class));
            finish();
        }

        init();
    }

    private void theme() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean("nightMode", false)) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }
    }

    public void init(){
        recyclerView = findViewById(R.id.menu1);
        recyclerView.setLayoutManager(new LinearLayoutManager(ActivityMenu.this,LinearLayoutManager.HORIZONTAL,false));
        cardArrayList = new ArrayList<>();
        adapterMateri = new AdapterMateri(cardArrayList, this);
        recyclerView.setAdapter(adapterMateri);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("materi");

        ref.child("Pemula").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot x: dataSnapshot.getChildren()) {
//                    imagesDir.add(imageSnapshot.child("address").getValue(String.class));
                    cardArrayList.add(new MateriCard("Pemula", x.getKey(),x.child("Judul").getValue().toString(),x.child("Deskripsi").getValue().toString(),x.child("author").getValue().toString()));
                    Log.d("LALALA -> " + x.getKey(),x.getValue().toString());
                }
                adapterMateri.notifyDataSetChanged();
//                Log.d("TSTT",dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR",databaseError.getMessage());
            }
        });

        rv2 = findViewById(R.id.menu2);
        rv2.setLayoutManager(new LinearLayoutManager(ActivityMenu.this,LinearLayoutManager.HORIZONTAL,false));
        ca2 = new ArrayList<>();
        am2 = new AdapterMateri(ca2,this);
        rv2.setAdapter(am2);

        ref.child("Lanjutan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot x: dataSnapshot.getChildren()) {
                    ca2.add(new MateriCard("Lanjutan", x.getKey(),x.child("Judul").getValue().toString(),x.child("Deskripsi").getValue().toString(),x.child("author").getValue().toString()));
                }
                am2.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR",databaseError.getMessage());
            }
        });

        rv3 = findViewById(R.id.menu3);
        rv3.setLayoutManager(new LinearLayoutManager(ActivityMenu.this,LinearLayoutManager.HORIZONTAL,false));
        ca3 = new ArrayList<>();
        am3 = new AdapterMateri(ca3,this);
        rv3.setAdapter(am3);

        ref.child("Mahir").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot x: dataSnapshot.getChildren()) {
                    ca3.add(new MateriCard("Mahir",x.getKey(),x.child("Judul").getValue().toString(),x.child("Deskripsi").getValue().toString(),x.child("author").getValue().toString()));
                }
                am3.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR",databaseError.getMessage());
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    void add(String id,String judul, String desc){
        cardArrayList.add(new MateriCard(id,judul,desc));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout :
                menAuth.signOut();
                startActivity(new Intent(ActivityMenu.this,MainActivity.class));
                finish();
                return true;

            case R.id.edProfile :
                startActivity(new Intent(ActivityMenu.this,ActivityProfile.class));
                return true;

            case R.id.dark:
//                Switch swBtn = findViewById(R.id.dark);
                SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
                boolean darkMode = prefs.getBoolean("nightMode", false);

                SharedPreferences.Editor editor = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
                editor.putBoolean("nightMode", !darkMode);
                editor.apply();

                if (darkMode) {
                    setTheme(R.style.dark);

                } else {
                    setTheme(R.style.light);
                }

                recreate();
                return true;

            default :
                return super.onOptionsItemSelected(item);
        }

    }
}
