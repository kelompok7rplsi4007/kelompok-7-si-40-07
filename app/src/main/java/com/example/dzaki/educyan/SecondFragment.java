package com.example.dzaki.educyan;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {


    public SecondFragment() {
        // Required empty public constructor
    }

    Button cancel, submit;
    DatabaseReference ref;
    EditText feedback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_second, container, false);

        ref = FirebaseDatabase.getInstance().getReference().child("feedback");

//        cancel = v.findViewById(R.id.cancelFB);
        submit = v.findViewById(R.id.submitFB);
        feedback = v.findViewById(R.id.inFeedback);

//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cancelFeedback(v);
//            }
//        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitFeedback(v);
            }
        });

        return v;
    }

    public void submitFeedback(View view){
        // memasukkan feedback kedalam database
        String push = ref.push().getKey();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        ref = ref.child(push);
        ref.child("feedback").setValue(feedback.getText().toString());
        ref.child("author").setValue(mAuth.getCurrentUser().getUid());
        Toast.makeText(getActivity(), "Feedback sent!", Toast.LENGTH_SHORT).show();
//        getActivity().finish();
    }

    public void cancelFeedback(View view){
        getActivity().finish();
    }

}
