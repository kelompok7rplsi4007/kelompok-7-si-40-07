package com.example.dzaki.educyan;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.example.dzaki.educyan.Card.AdapterMateri;
import com.example.dzaki.educyan.Card.MateriCard;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class ActivityTeacher extends AppCompatActivity {
    FirebaseAuth menAuth;
    Dialog dialog;
    RadioGroup jenis;
    EditText judul, desk;

    RecyclerView recyclerView, rv2, rv3;
    ArrayList<MateriCard> cardArrayList, ca2,ca3;
    AdapterMateri adapterMateri, am2, am3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        theme();
        setContentView(R.layout.activity_teacher);
//        startActivity(new Intent(ActivityTeacher.this,AwalActivity.class));

        menAuth = FirebaseAuth.getInstance();

        if (menAuth.getCurrentUser() == null) {
            startActivity(new Intent(ActivityTeacher.this, MainActivity.class));
            finish();
        }

        initX();
    }

//    public void init(){
//        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("materi");
//        try{
//            listMateri="";
//            ref.addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                    if (dataSnapshot.exists()) {
//                        for (DataSnapshot uniqueUserSnapshot : dataSnapshot.getChildren()) {
////                            listMateri += "Judul\t:\t" + uniqueUserSnapshot.child("Judul").getValue().toString()+"\nDeskripsi\t:\t" + uniqueUserSnapshot.child("Deskripsi").getValue().toString()+"\n-----------------\n";
//                        }
//                        materi.setText(listMateri);
////                   }
//
//                    }
//                }
//
//                @Override
//                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                }
//            });
//        }catch(Exception e){
//
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void theme() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if (prefs.getBoolean("nightMode", false)) {
            setTheme(R.style.dark);
        } else {
            setTheme(R.style.light);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                menAuth.signOut();
                startActivity(new Intent(ActivityTeacher.this, MainActivity.class));
                finish();
                return true;

            case R.id.edProfile:
                startActivity(new Intent(ActivityTeacher.this, ActivityProfile.class));
                return true;

            case R.id.dark:
                SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
                boolean darkMode = prefs.getBoolean("nightMode", false);

                SharedPreferences.Editor editor = getSharedPreferences(getPackageName(), MODE_PRIVATE).edit();
                editor.putBoolean("nightMode", !darkMode);
                editor.apply();

                if (darkMode) {
                    setTheme(R.style.dark);

                } else {
                    setTheme(R.style.light);
                }

                recreate();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void add(View view) {

        dialog = new Dialog(view.getContext());
        dialog.setContentView(R.layout.tambah);
        jenis = dialog.findViewById(R.id.jenisMateri);

        judul = dialog.findViewById(R.id.edJudul);
        desk = dialog.findViewById(R.id.edDeskripsi);

        Button tambah, batal;

        tambah = dialog.findViewById(R.id.addMateri);
        batal = dialog.findViewById(R.id.cancel);



        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton hx = dialog.findViewById(jenis.getCheckedRadioButtonId());
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("materi").child(hx.getText().toString());
                String ndx = ref.push().getKey();
                ref = ref.child(ndx);
                ref.child("Judul").setValue(judul.getText().toString());
                ref.child("Deskripsi").setValue(desk.getText().toString());
                ref.child("author").setValue(menAuth.getCurrentUser().getUid());
                initX();
                dialog.dismiss();
            }
        });


        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    public void initX(){
        recyclerView = findViewById(R.id.menu1);
        recyclerView.setLayoutManager(new LinearLayoutManager(ActivityTeacher.this,LinearLayoutManager.HORIZONTAL,false));
        cardArrayList = new ArrayList<>();
        adapterMateri = new AdapterMateri(cardArrayList, this);
        recyclerView.setAdapter(adapterMateri);

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("materi");

        ref.child("Pemula").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot x: dataSnapshot.getChildren()) {
//                    imagesDir.add(imageSnapshot.child("address").getValue(String.class));
                    cardArrayList.add(new MateriCard("Pemula", x.getKey(),x.child("Judul").getValue().toString(),x.child("Deskripsi").getValue().toString(),x.child("author").getValue().toString()));
                    Log.d("LALALA -> " + x.getKey(),x.getValue().toString());
                }
                adapterMateri.notifyDataSetChanged();
//                Log.d("TSTT",dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR",databaseError.getMessage());
            }
        });

        rv2 = findViewById(R.id.menu2);
        rv2.setLayoutManager(new LinearLayoutManager(ActivityTeacher.this,LinearLayoutManager.HORIZONTAL,false));
        ca2 = new ArrayList<>();
        am2 = new AdapterMateri(ca2,this);
        rv2.setAdapter(am2);

        ref.child("Lanjutan").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot x: dataSnapshot.getChildren()) {
                    ca2.add(new MateriCard("Lanjutan", x.getKey(),x.child("Judul").getValue().toString(),x.child("Deskripsi").getValue().toString(),x.child("author").getValue().toString()));
                }
                am2.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR",databaseError.getMessage());
            }
        });

        rv3 = findViewById(R.id.menu3);
        rv3.setLayoutManager(new LinearLayoutManager(ActivityTeacher.this,LinearLayoutManager.HORIZONTAL,false));
        ca3 = new ArrayList<>();
        am3 = new AdapterMateri(ca3,this);
        rv3.setAdapter(am3);

        ref.child("Mahir").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot x: dataSnapshot.getChildren()) {
                    ca3.add(new MateriCard("Mahir", x.getKey(),x.child("Judul").getValue().toString(),x.child("Deskripsi").getValue().toString(),x.child("author").getValue().toString()));
                }
                am3.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERRROR",databaseError.getMessage());
            }
        });

    }
}
